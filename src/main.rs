// Importacion de las bibliotecas necesarias
use handlebars::Handlebars;
use serde::{Deserialize, Serialize};
use serde_json::{self};
use std::io::Write;
use std::{fs::File, io::Read};

/*
Definicion de las estructuras que se van a utilizar
se usan para parsear el archivo settings.json y para renderizar el archivo base.html
 */
#[derive(Debug, Deserialize, Serialize)]
struct FileConfig {
    base_name: String,
    title: String,
    out_file: String,
    routes: Vec<Routes>,
}

/*
Serde(default) se usa para que si no se encuentra el campo en el archivo settings.json
se use el valor por defecto que se le asigna a la variable
 */
#[derive(Debug, Deserialize, Serialize)]
struct Routes {
    name: String,
    #[serde(default)]
    img_src: Option<String>,
    url: String,
    #[serde(default)]
    text: Option<String>,
}
// Función principal
fn main() {
    // Abrir el archivo settings.json y manejar el error si no se encuentra
    let file: File = File::open("./settings.json").unwrap_or_else(|_| {
        panic!("Por favor crea un archivo settings.json en la raiz del proyecto.")
    });
    // Leer y analizar el archivo settings.json directamente en la estructura FileConfig
    let settings: FileConfig = serde_json::from_reader(file)
        .expect("No se pueden cargar los datos del archivo settings.json, por favor revisa la sintaxis y vuelve a intentarlo");

    // Crear una instancia de Handlebars la cual se usara para renderizar el archivo base.html
    let h = Handlebars::new();

    // Abrir el archivo base.html y manejar el error si no se encuentra
    let mut base_file: File = File::open(&settings.base_name)
        .expect("No se puede abrir el archivo base.html en la raiz del proyecto por alguna razon");
    // Crear una variable temporal para almacenar el contenido del archivo base.html en ella
    let mut temp_string = String::new();
    // Leer el archivo base.html y almacenar el contenido en la variable temp_string
    base_file
        .read_to_string(&mut temp_string)
        .expect("No se puede leer el archivo base.html en la raiz del proyecto, verifica que exista y que tenga permisos de lectura");
    // Renderizar el archivo base.html con los datos del archivo settings.json
    let rendered_file = h.render_template(temp_string.as_str(), &settings);
    // Abrir el archivo de salida y manejar el error si no se encuentra
    let mut out_file: File = File::create(settings.out_file).expect("No se puede crear el archivo de salida");
    out_file
        .write(rendered_file.unwrap().as_bytes())
        .expect("No se puede escribir en el archivo de salida");
}
