# Generador de plantillas Handlebars

Este programa utiliza la biblioteca Handlebars para generar un archivo HTML a partir de una plantilla y datos proporcionados en un archivo `settings.json`.

## Funcionamiento

1. El programa lee un archivo `settings.json` en la raíz del proyecto, que contiene información sobre la plantilla base, el título, el archivo de salida y las rutas.
2. La plantilla base se abre y se lee en una cadena.
3. Handlebars renderiza la plantilla utilizando los datos proporcionados en `settings.json`.
4. El archivo HTML generado se escribe en un archivo de salida especificado en `settings.json`.

## Estructura del archivo settings.json

El archivo `settings.json` debe contener la siguiente estructura:

```json
{ 
    "base_name": "ruta/al/archivo/base.html",
    "title": "Título de la pagina web",
    "out_file": "ruta/al/archivo/salida.html",
    "routes": [ 
            { 
                "name": "Nombre de la ruta", 
                "img_src": "URL de la imagen (opcional)",
                "url": "URL de la ruta",
                "text": "Texto de la ruta (opcional)" 
            } 
        ] 
}
```

### Instalación de Rust

Para instalar Rust, siga las instrucciones en la [página oficial de Rust](https://www.rust-lang.org/tools/install). Esto instalará el administrador de paquetes `cargo` y el compilador `rustc`.

### Compilar el proyecto

Para compilar el proyecto, navegue hasta la carpeta del proyecto utilizando la línea de comandos y ejecute el siguiente comando:

`cargo build`

Esto generará un ejecutable en la carpeta `target/debug`.

### Crear una versión de release

Para crear una versión de release optimizada, ejecute el siguiente comando:

`cargo build --release`

Esto generará un ejecutable optimizado en la carpeta `target/release`.

### Añadir dependencias

Para añadir dependencias al proyecto, edite el archivo `Cargo.toml` y agregue las dependencias necesarias en la sección `[dependencies]`. Por ejemplo:

```toml
[dependencies]
serde = {version="1.0.163",features = ["derive"]}
serde_json = "1.0"
handlebars = "4.3.7"
```

### Archivo settings.json y salida

El ejecutable generado requiere un archivo `settings.json` en la misma carpeta que el ejecutable. La salida del archivo se generará en la misma ubicación que el ejecutable.

### Enlaces a bibliotecas

Para obtener más información sobre las bibliotecas utilizadas en este proyecto, consulte los siguientes enlaces:

1. [Handlebars](https://docs.rs/handlebars/3.5.2/handlebars/)
2. [Serde](https://serde.rs/)

### Información para principiantes en programación

Este programa utiliza la biblioteca Handlebars para generar un archivo HTML a partir de una plantilla y datos proporcionados en un archivo `settings.json`. Handlebars es una biblioteca que permite crear plantillas HTML dinámicas utilizando datos proporcionados en un formato específico. Serde es otra biblioteca utilizada en este programa para manejar la serialización y deserialización de datos en formato JSON. Al ejecutar el programa, se leerá el archivo `settings.json`, se procesará la plantilla HTML y se generará un archivo de salida con el contenido renderizado.
